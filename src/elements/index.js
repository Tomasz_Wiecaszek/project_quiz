import moto from "../assets/moto.png";
import culture from "../assets/culture.png";
import science from "../assets/science.png";
import history from "../assets/history.png";
import prog from "../assets/prog.png";

export const myCatList = [
  { icon: science, title: "Technologia", link: "/Technologia" },
  { icon: culture, title: "Kultura", link: "/Kultura" },
  { icon: moto, title: "Motoryzacja", link: "/Motoryzacja" },
  { icon: prog, title: "Programowanie", link: "/Programowanie" },
  { icon: history, title: "Historia", link: "/Historia" },
];

export const questions = {
  Technologia: {
    1: {
      type: "normal",
      quest: "Jak nazywa się największy na świecie akcelerator cząstek?",
      answers: [
        "Wielki zderzak atomów",
        "Wielki zderzacz irytonów",
        "Wielki pogromca wampirow",
        "Wielki zderzacz hadronów",
        "Big Boy Atom Sevilla",
      ],
      correct: "Wielki zderzacz hadronów",
    },
    2: {
      type: "normal",
      quest:
        "Która marka odpowiada za serię urządzeń ze słynnym 'i' w nazwie ?",
      answers: ["Microsoft", "Huawei", "Apple", "Cisco", "Tele5"],
      correct: "Apple",
    },
    3: {
      type: "dd-list",
      quest: "Ułóż wydarzenia w kolejności chronologicznej",
      answers: [
        "Wynalezienie żarówki",
        "Powstanie sieci internet",
        "Wynalezienie koła",
        "Pierwszy lot w kosmos",
      ],
      correct: [
        "Wynalezienie koła",
        "Wynalezienie żarówki",
        "Powstanie sieci internet",
        "Pierwszy lot w kosmos",
      ],
    },
    4: {
      type: "normal",
      quest: "Jak nazywa się największy teleskop na świecie?",
      answers: [
        "Teelskop Hubble`a",
        "Gran Telescopio Canarias",
        "Wielki Teleskop Pekiński",
        "Grand U.S. TelSco",
        "GrandeTele",
      ],
      correct: "Gran Telescopio Canarias",
    },
    5: {
      type: "dd-place",
      quest: ["Żarówki dzielą się na...", " i tradycyjne."],
      answers: [
        "A. Żarowe",
        "B. Energooszczędne",
        "C. Energiczne",
        "D. Fluorowe",
        "E. Chemiczne",
      ],
      correct: "B. Energooszczędne",
    },
    6: {
      type: "normal",
      quest: "Jak nazywa się najmniejszy procesor na świecie?",
      answers: [
        "Kinetis KL02",
        "Icore-i1",
        "AMD-small-x443",
        "SnapDragon 34443",
        "SapphireSmallx",
      ],
      correct: "Kinetis KL02",
    },
    7: {
      type: "normal",
      quest: "Ile USD zarabia rocznie firma micrsoft?",
      answers: [
        "300 mln",
        "ponad 500 mln",
        "ponad 1 mld",
        "ponad 2 mld",
        "10 mld",
      ],
      correct: "ponad 2 mld",
    },
    8: {
      type: "dd-list",
      quest:
        "Ułóż producentów w kolejności od najmniej do najwięcej zarabiającego",
      answers: ["Sony", "Huawei", "Apple", "Samsung"],
      correct: ["Apple", "Samsung", "Huawei", "Sony"],
    },
    9: {
      type: "dd-place",
      quest: ["Spółka...", " powstała 6 kwietnia 2010 roku w Pekinie?"],
      answers: ["A. Huawei", "B. OppO", "C. SmartTel", "D. Xiaomi", "E. Sony"],
      correct: "D. Xiaomi",
    },
    10: {
      type: "normal",
      quest: "Jak nazywa się największy producent monitorów?",
      answers: ["Boe Technology", "LG", "DELL", "AEC", "Samsung"],
      correct: "Boe Technology",
    },
  },

  Kultura: {
    1: {
      type: "normal",
      quest: "Który teatr operowy ma największą na świecie widownię?",
      answers: [
        "Metropolitan Opera House",
        "Opera w Sydney",
        "Teatro alla Scala",
        "Royal Opera House",
        "San Antonio Opera",
      ],
      correct: "Metropolitan Opera House",
    },
    2: {
      type: "normal",
      quest:
        "Jaki aktor został nagrodony w 1949 Oscarem dwukrotnie za rolę bohaterow szekspirowskich?",
      answers: [
        "Michael Douglas",
        "Sean Connery",
        "Laurence Olivier",
        "Michael Caine",
        "Sean Huwards",
      ],
      correct: "Laurence Olivier",
    },
    3: {
      type: "dd-list",
      quest: "Ułóż teatry od największego do najmniejszego: ",
      answers: [
        "San Francisco Opera",
        "Metropolitan Opera House",
        "Gran Teatre del Lilleu",
        "Teatro alla Scala",
      ],
      correct: [
        "Metropolitan Opera House",
        "San Francisco Opera",
        "Teatro alla Scala",
        "Gran Teatre del Lilleu",
      ],
    },
    4: {
      type: "normal",
      quest:
        "Jak nazywa się balet, którego bohaterami są Odetta i książe Zygfryd ?",
      answers: [
        "Jezioro Łabędzie",
        "Dziadek do orzechów",
        "Śpiąca królewna",
        "DD Cane Balette",
        "Tajemniczy ogród",
      ],
      correct: "Jezioro Łabędzie",
    },
    5: {
      type: "dd-place",
      quest: [
        "Wit...",
        " stworzył nastawę na ołtarz w krakowskim kościele mariackim.",
      ],
      answers: [
        "A. Witalij",
        "B. Stępielecki",
        "C. Stwosz",
        "D. Kondrat",
        "E. Luis",
      ],
      correct: "C. Stwosz",
    },
    6: {
      type: "normal",
      quest: "Jak nazywa się pierwszy musical rockowy w historii?",
      answers: [
        "Chicago Dance",
        "Hair",
        "Just Dance",
        "West Side Story",
        "Coco",
      ],
      correct: "Hair",
    },
    7: {
      type: "normal",
      quest: "Kto namalował Panny z Avignon?",
      answers: ["Goya", "Da Vinci", "Velasquez", "Picasso", "Rembrandt"],
      correct: "Picasso",
    },
    8: {
      type: "dd-list",
      quest: "Ułóż epoki literacki od najwcześniejszej do najpóźniejszej",
      answers: ["Barok", "Renesans", "Romantyzm", "Oświecenie"],
      correct: ["Renesans", "Barok", "Oświecenie", "Romantyzm"],
    },
    9: {
      type: "dd-place",
      quest: ["Dama z .....", " to słynny obraz Leonarda Da Vinci."],
      answers: ["Pieskiem", "Gronostajem", "mokrą głową", "Wenecji"],
      correct: "Gronostajem",
    },
    10: {
      type: "normal",
      quest: "W jakim państwie znajduje się zamek Alhambra?",
      answers: ["Grecja", "Hiszpania", "Francja", "Włochy", "USA"],
      correct: "Hiszpania",
    },
  },
  Motoryzacja: {
    1: {
      type: "normal",
      quest: "Do jakiego koncernu należy obecnie marka Opel?",
      answers: ["VAG", "GM", "PSA", "TATA", "Japan Moto"],
      correct: "PSA",
    },
    2: {
      type: "normal",
      quest: "Jaki klucz służy do dokręcenia śrub odpowiednią siłą?",
      answers: [
        "Dynamiczny",
        "Dynamometryczny",
        "Udarowy",
        "Niutonometryczny",
        "Wiolinowy",
      ],
      correct: "Dynamometryczny",
    },
    3: {
      type: "dd-list",
      quest:
        "Ułóż podzespoły samochodu w kolejności od największego do najmniejszego",
      answers: [
        "Akumulator",
        "Przekładnia kierownicza",
        "czujnik ciśnienia oleju",
        "Koło zmiennych faz rozrządu",
      ],
      correct: [
        "Przekładnia kierownicza",
        "Akumulator",
        "Koło zmiennych faz rozrządu",
        "czujnik ciśnienia oleju",
      ],
    },
    4: {
      type: "normal",
      quest:
        "Ile cylindrów posiada silnik BMW serii 7 z 2000 roku o oznaczeniu 740 i ?",
      answers: ["4", "6", "8", "12", "14"],
      correct: "8",
    },
    5: {
      type: "dd-place",
      quest: [
        "W silniku samochodu układ...",
        " odpowiada za synchroniację wału napędowego z wałem rozrządczym.",
      ],
      answers: [
        "A. Zapłonowy",
        "B. Wydechowy",
        "C. Elektronicznego ustawiania kąta",
        "D. Rozrządu",
        "E. Scalony",
      ],
      correct: "D. Rozrządu",
    },
    6: {
      type: "normal",
      quest: "Jaki typ sprzęgła posiadają samochody marki Subaru Impreza?",
      answers: [
        "Dociskowe",
        "Wyciskowe",
        "Ciągnione",
        "Pchane",
        "Hydro-elektryczne",
      ],
      correct: "Ciągnione",
    },
    7: {
      type: "normal",
      quest:
        "Jaka jest minimalna wysokość bieżnika dla opony zimowej, poniżej której traci swoje właściwości w zimie?",
      answers: ["2mm", "3mm", "4mm", "5mm", "7mm"],
      correct: "4mm",
    },
    8: {
      type: "dd-list",
      quest:
        "Ułóż samochody według pojemności silnika od najmniejszej do największej.",
      answers: [
        "Mercedes s320(2004r)",
        "VW LT 35",
        "BMW 320 (1997 r.)",
        "Audi 80 Competiton",
      ],
      correct: [
        "Audi 80 Competiton",
        "BMW 320 (1997 r.)",
        "VW LT 35",
        "Mercedes s320(2004r)",
      ],
    },
    9: {
      type: "dd-place",
      quest: ["Lancia...", " to odpowiednik modelu Chryslera 300C."],
      answers: ["Delta", "Phedra", "Thema", "Ypsilon", "Delta"],
      correct: "Thema",
    },
    10: {
      type: "normal",
      quest:
        "Która marka posiada w swojej ofercie samochody zarówno osobowe, dostawcze i ciężarowe?",
      answers: ["Opel", "VW", "MAN", "Mercedes", "Suzuki"],
      correct: "Mercedes",
    },
  },

  Programowanie: {
    1: {
      type: "normal",
      quest:
        "Jak nazywa się język używany w większości po stronie klienta w aplkacjach internetowych?",
      answers: ["Java", "JavaScript", "C++", "C#", "Python"],
      correct: "JavaScript",
    },
    2: {
      type: "normal",
      quest: "Jak zadeklarować stałą w JavaScript?",
      answers: ["var", "let", "const", "string", "null"],
      correct: "const",
    },

    3: {
      type: "dd-list",
      quest:
        "Ułóż w kolejności czynności dotyczące tworzenia programu w języku C++",
      answers: [
        "Testowanie",
        "Pisanie kodu",
        "Uruchomienie programu",
        "Kompilacja",
      ],
      correct: [
        "Pisanie kodu",
        "Testowanie",
        "Kompilacja",
        "Uruchomienie programu",
      ],
    },
    4: {
      type: "normal",
      quest:
        "Jakie rozszerzenie posiadają pliki z gotowym do uruchomienia programem napisanym w JAVA?",
      answers: [".exe", ".java", ".cmd", ".dll", "php"],
      correct: ".java",
    },
    5: {
      type: "dd-place",
      quest: ["Język o nazwie...", " jest jezykiem najniższego poziomu?"],
      answers: ["A. C", "B. Assembler", "C. C#", "D. Python", "E. HTML"],
      correct: "B. Assembler",
    },
    6: {
      type: "normal",
      quest:
        "Jakie rozszerzenie posiadają biblioteki z dodatkowym kodem do plików wykonywalnych .exe ?",
      answers: [".git", ".core", ".dll", ".win", ".ini"],
      correct: ".dll",
    },
    7: {
      type: "normal",
      quest: "Jak zwiększyć liczbę o 1 za pomocą postinkrementacji?",
      answers: ["a++", "++a", "a+a", "+a+", "-a-"],
      correct: "a++",
    },
    8: {
      type: "dd-list",
      quest:
        "Ułóż wyrażenia w kolejności od najmniejszego do największego, kiedy a = 2 , b = 3",
      answers: ["a%b", "b+b", "a+b", "b%a"],
      correct: ["b%a", "a%b", "a+b", "b+b"],
    },
    9: {
      type: "dd-place",
      quest: ["Wpisując w kodzie HTML...", " rozpoczniemy komentarz."],
      answers: ["A. <!--", "B. //", "C. </>", "D. \\", "E. >>?"],
      correct: "A. <!--",
    },
    10: {
      type: "normal",
      quest: "Jakiego języka dotyczy framework Laravel ? ",
      answers: ["JS", "C++", "PHP", "C#", "bash"],
      correct: "PHP",
    },
  },

  Historia: {
    1: {
      type: "normal",
      quest: "Jak nazywał się pierwszy król Polski ?",
      answers: [
        "Mieszko I",
        "Bolesław Chrobry",
        "Kazimierz Wielki",
        "Stefan Batory",
        "Jan III Waza",
      ],
      correct: "Bolesław Chrobry",
    },
    2: {
      type: "normal",
      quest: "W którym roku rozegrała się Bitwa pod Grunwaldem ?",
      answers: ["966", "1200", "1410", "1674", "1233"],
      correct: "1410",
    },
    3: {
      type: "dd-list",
      quest: "Ułóż wydarzenia w kolejności chronologicznej",
      answers: [
        "Uchwalenie Konstytucji",
        "chrzest Polski",
        "Zjazd Gnieźnieński",
        "2 wojna światowa",
      ],
      correct: [
        "chrzest Polski",
        "Zjazd Gnieźnieński",
        "Uchwalenie Konstytucji",
        "2 wojna światowa",
      ],
    },
    4: {
      type: "normal",
      quest: "Jakie miasto stanowiło główną siedzibę zakonu krzyżackiego?",
      answers: ["Warszawa", "Malbork", "Lublin", "Gniezno", "Lubin"],
      correct: "Malbork",
    },
    5: {
      type: "dd-place",
      quest: [
        "Król Polski, który nazywał się...",
        " dokonał rewolucji budowlanej z którą związane jest znane powiedzenie ?",
      ],
      answers: [
        "A. Kazimierz Wielki",
        "B. Stefan Batory",
        "C. Władysław Łokietek",
        "D. Kazimierz Jagiellończyk",
        "E. Stefan Wielki",
      ],
      correct: "A. Kazimierz Wielki",
    },
    6: {
      type: "normal",
      quest:
        "Którego dnia Września niemiecki pancernik dokonał ostrzału Westerplatte?",
      answers: ["1", "2", "3", "4", "6"],
      correct: "1",
    },
    7: {
      type: "normal",
      quest: "Kto dokonał odkrycia Ameryki ?",
      answers: ["Kolumb", "Velasquez", "Da Gama", "Da Vinci", "Depardieu"],
      correct: "Kolumb",
    },
    8: {
      type: "dd-list",
      quest: "Ułóż wydarzenia w kolejności chronologicznej",
      answers: [
        "Komunizm w Polsce",
        "Atak na WTC",
        "Wybuch 2 wojny światowej",
        "Wybuch 1 wojny światowej",
      ],
      correct: [
        "Wybuch 1 wojny światowej",
        "Wybuch 2 wojny światowej",
        "Komunizm w Polsce",
        "Atak na WTC",
      ],
    },
    9: {
      type: "dd-place",
      quest: ["Miasto...", " stanowiło drugą stolicę Polski ?"],
      answers: [
        "A. Gniezno",
        "B. Warszawa",
        "C. Kraków",
        "D. Poznań",
        "E. Bydgoszcz",
      ],
      correct: "C. Kraków",
    },
    10: {
      type: "normal",
      quest:
        "Jak nazywa najsłynniejszy karabin maszynowy używany od 2 wojny światowej?",
      answers: ["AK 47", "Peter", "PK", "Beryl", "BigB"],
      correct: "AK 47",
    },
  },

  // 'category':"culture",
  // 'category':'moto',
  // 'category':'prog',
  // 'category':'history'
};

export default { myCatList, questions };
