import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import * as React from "react";
import { Suspense } from "react";
import "./styles/themes/default/theme.scss";
import Homepage from "./pages/homepage";
import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

const Technology = React.lazy(() => import("./pages/tech"));
const Culture = React.lazy(() => import("./pages/culture"));
const Moto = React.lazy(() => import("./pages/moto"));
const History = React.lazy(() => import("./pages/history"));
const Prog = React.lazy(() => import("./pages/prog"));

function App() {
  return (
    <Router>
      <Switch>
        <Suspense
          fallback={
            <Loader
              type="Puff"
              color="#00BFFF"
              height={100}
              width={100}
              timeout={3000}
            />
          }
        >
          <Route exact path="/" component={Homepage} />
          <Route path="/Technologia" component={Technology} />
          <Route path="/Kultura" component={Culture} />
          <Route path="/Motoryzacja" component={Moto} />
          <Route path="/Historia" component={History} />
          <Route path="/Programowanie" component={Prog} />
        </Suspense>
      </Switch>
    </Router>
  );
}

export default App;
