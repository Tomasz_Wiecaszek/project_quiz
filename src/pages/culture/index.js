import React from "react";
import { myCatList } from "../../elements";
import Hello from "../../components/Hello";
import { useState } from "react";
const Culture = () => {
  let [invertBackground, setInvertBackground] = useState(false);
  return (
    <div
      className={`main-wrapper__culture ${
        invertBackground ? "invertBackground" : ""
      }`}
    >
      <div className="main-wrapper__container">
        <Hello el={myCatList[1]} setInvertBackground={setInvertBackground} />
      </div>
    </div>
  );
};

export default Culture;
