import { React, useState } from "react";
import { myCatList } from "../../elements";
import Hello from "../../components/Hello";

const Tech = () => {
  let [invertBackground, setInvertBackground] = useState(false);

  return (
    <div
      className={`main-wrapper__tech ${
        invertBackground ? "invertBackground" : ""
      }`}
    >
      <div className="main-wrapper__container">
        <Hello el={myCatList[0]} setInvertBackground={setInvertBackground} />
      </div>
    </div>
  );
};

export default Tech;
