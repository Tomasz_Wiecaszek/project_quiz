import { React, useState } from "react";
import { myCatList } from "../../elements";
import Hello from "../../components/Hello";

const Moto = () => {
  let [invertBackground, setInvertBackground] = useState(false);

  return (
    <div
      className={`main-wrapper__moto ${
        invertBackground ? "invertBackground" : ""
      }`}
    >
      <div className="main-wrapper__container">
        <Hello el={myCatList[2]} setInvertBackground={setInvertBackground} />
      </div>
    </div>
  );
};

export default Moto;
