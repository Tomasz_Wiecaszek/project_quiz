import { React, useState } from "react";
import { myCatList } from "../../elements";
import Hello from "../../components/Hello";

const Prog = () => {
  let [invertBackground, setInvertBackground] = useState(false);
  return (
    <div
      className={`main-wrapper__prog ${
        invertBackground ? "invertBackground" : ""
      }`}
    >
      <div className="main-wrapper__container">
        <Hello el={myCatList[3]} setInvertBackground={setInvertBackground} />
      </div>
    </div>
  );
};

export default Prog;
