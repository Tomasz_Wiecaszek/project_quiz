import Title from "../../components/Title";
import Tip from "../../components/Tip";
import Categories from "../../components/Categories";

const Homepage = () => {
  return (
    <div className="main-wrapper__homepage">
      <div className="main-wrapper__container">
        <Title backButton={false} />
        <Tip txt="10 PYTAŃ / 5 KATEGORII" theme="Technologia" />

        <Categories title="Homepage" />
      </div>
    </div>
  );
};

export default Homepage;
