import { React, useState } from "react";
import { myCatList } from "../../elements";
import Hello from "../../components/Hello";

const History = () => {
  let [invertBackground, setInvertBackground] = useState(false);
  return (
    <div
      className={`main-wrapper__history ${
        invertBackground ? "invertBackground" : ""
      }`}
    >
      <div className="main-wrapper__container">
        <Hello el={myCatList[4]} setInvertBackground={setInvertBackground} />
      </div>
    </div>
  );
};

export default History;
