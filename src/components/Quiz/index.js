import { questions } from "../../elements";
import { useState } from "react";
import Tip from "../../components/Tip";
import ResultCircle from "../../components/ResultCircle";
import Result from "../Result";
import StandardQuestion from "../StandardQuestion";
import DragAndDropQuestion from "../DragAndDropQuestion";
import DragAndDropQuestionPlace from "../DragAndDropQuestionPlace";

const Quiz = ({
  category,
  icon,
  title,
  setResultTitle,
  setBackButton,
  setInvertBackground,
}) => {
  //zmienne, stany

  setBackButton(true);
  setInvertBackground(true);
  let [counter, setCounter] = useState(1);
  let [correctCounter, setCorrectCounter] = useState(0);
  let [result, setResult] = useState(false);
  let question = "";

  //switch ustalający odpowiedni obiekt
  switch (category) {
    case "Technologia":
      question = questions.Technologia[counter];
      break;
    case "Kultura":
      question = questions.Kultura[counter];
      break;
    case "Motoryzacja":
      question = questions.Motoryzacja[counter];
      break;
    case "Programowanie":
      question = questions.Programowanie[counter];
      break;
    case "Historia":
      question = questions.Historia[counter];
      break;
    default:
      question = questions.Technologia[counter];
      break;
  }
  //funckja resetująca quiz dla przycisku 'powtorz quiz'
  const resetQuiz = () => {
    setResult(false);
    setCorrectCounter(0);
    setCounter(1);
    setResultTitle("");
  };

  return (
    <div>
      {
        !result ? (
          <>
            <div className="quiz__header">
              <Tip
                theme={category}
                txt={
                  question.type === "normal"
                    ? "Wybierz poprawną odpowiedź"
                    : question.type === "dd-list"
                    ? "Przeciągaj i upuszczaj, aby ułożyc w odpowiedniej kolejności"
                    : "Wybierz prawidłową odpowiedź i przeciągnij we wskazane miejsce"
                }
              />
              <ResultCircle result={counter} theme={category} />
            </div>

            {question.type === "normal" ? (
              <StandardQuestion
                question={question}
                category={category}
                counter={counter}
                setCounter={setCounter}
                correctCounter={correctCounter}
                setCorrectCounter={setCorrectCounter}
                setResult={setResult}
              />
            ) : question.type === "dd-list" ? (
              <DragAndDropQuestion
                question={question}
                category={category}
                counter={counter}
                setCounter={setCounter}
                correctCounter={correctCounter}
                setCorrectCounter={setCorrectCounter}
              />
            ) : (
              <DragAndDropQuestionPlace
                question={question}
                category={category}
                counter={counter}
                setCounter={setCounter}
                correctCounter={correctCounter}
                setCorrectCounter={setCorrectCounter}
                setResult={setResult}
              />
            )}
          </>
        ) : (
          <Result
            setBackButton={setBackButton}
            theme={category}
            result={correctCounter}
            icon={icon}
            title={title}
            resetQuiz={resetQuiz}
            setResultTitle={setResultTitle}
            setInvertBackground={setInvertBackground}
          />
        )
        //wyświetlanie po ostatnim pytaniu
      }
    </div>
  );
};

export default Quiz;
