const Tip = ({ txt, theme, endResultTip }) => {
  return (
    <div className={`tip_wrapper`}>
      <div className={`main-wrapper__tip-${theme} ${endResultTip}`}>
        <p>
          <i>{txt}</i>
        </p>
      </div>
    </div>
  );
};

export default Tip;
