import { React, useState } from "react";

const StandardQuestion = ({
  question,
  category,
  counter,
  setCounter,
  correctCounter,
  setCorrectCounter,
  setResult,
}) => {
  let [good, setGood] = useState([false, false, false, false]);
  let [bad, setBad] = useState([false, false, false, false]);
  let [disabled, setDisabled] = useState(false);
  const letters = ["A", "B", "C", "D", "E"];

  const check = (answer, index) => {
    if (answer === question.correct) {
      let goodNew = good.splice();
      goodNew[index] = true;
      setGood(goodNew);
      setCorrectCounter(correctCounter + 1);
      setDisabled(true);
    } else {
      let badNew = bad.splice();
      badNew[index] = true;
      setBad(badNew);
      setDisabled(true);
    }
    if (counter < 10) {
      setTimeout(() => {
        setDisabled(false);
        setGood([false, false, false, false]);
        setBad([false, false, false, false]);
        setCounter(counter + 1);
      }, 500);
    } else {
      setTimeout(() => {
        setDisabled(false);
        setResult(true);
      }, 500);
    }
  };

  return (
    <>
      <div className="quiz__question">{question.quest}</div>

      <div className="answers">
        {question.answers.map((el, index) => {
          return (
            <button
              disabled={disabled}
              className={`startButton__${category}
${good[index] ? "good" + index : ""} ${bad[index] ? "bad" + index : ""} 
`}
              onClick={() => {
                check(el, index);
              }}
              key={Math.random()}
            >
              <p>
                {letters[index]}. {el}
              </p>
            </button>
          );
        })}
      </div>
    </>
  );
};

export default StandardQuestion;
