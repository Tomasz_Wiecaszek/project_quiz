import React, { useState } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

const DragAndDropQuestion = ({
  question,
  category,
  counter,
  setCounter,
  correctCounter,
  setCorrectCounter,
}) => {
  const [actualAnswers, updateActualAnswers] = useState(question.answers);

  let [isGood, setIsGood] = useState(false);
  let [isBad, setIsBad] = useState(false);
  let [disabled, setDisabled] = useState(false);
  function handleOnDragEnd(result) {
    if (!result.destination) return;

    const items = Array.from(actualAnswers);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);
    updateActualAnswers(items);
  }

  const check = () => {
    if (JSON.stringify(actualAnswers) === JSON.stringify(question.correct)) {
      setDisabled(true);
      setCorrectCounter(correctCounter + 1);
      setIsGood(true);

      setTimeout(() => {
        setCounter(counter + 1);
        setDisabled(false);
      }, 1000);
    } else {
      setIsBad(true);
      setDisabled(true);
      setTimeout(() => {
        setCounter(counter + 1);
        setDisabled(false);
      }, 1000);
    }
  };

  return (
    <div>
      <div className="quiz__question">{question.quest}</div>

      <DragDropContext onDragEnd={handleOnDragEnd}>
        <Droppable droppableId="questions">
          {(provided) => (
            <div
              className="AnswersDD"
              {...provided.droppableProps}
              ref={provided.innerRef}
            >
              {actualAnswers.map((el, index) => {
                return (
                  <Draggable
                    key={Math.random()}
                    index={index}
                    draggableId={el}
                    isDragDisabled={disabled}
                  >
                    {(provided) => (
                      <div
                        className={`startButton__${category} AnswersDD__Drag`}
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                      >
                        <p>{el}</p>
                      </div>
                    )}
                  </Draggable>
                );
              })}
              {provided.placeholder}
              <hr></hr>
              <button
                className={`startButton__${category} AnswersDD__check 
${isGood ? "good0" : ""} ${isBad ? "bad0" : ""}
`}
                onClick={() => {
                  check();
                }}
              >
                <p>Sprawdz</p>
              </button>
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </div>
  );
};

export default DragAndDropQuestion;
