const ResultCircle = ({ result, theme, endResultCircle }) => {
  return (
    <div className={`quiz__resultCircle-${theme} ${endResultCircle}`}>
      <p className="quiz__resultCircle--Title">TWOJ WYNIK</p>
      <p className="quiz__resultCircle--wrapper">
        {result}/<b className="quiz__resultCircle--numb">10</b>
      </p>
    </div>
  );
};

export default ResultCircle;
