import { React, useState } from "react";
import Title from "../../components/Title";
import Tip from "../../components/Tip";
import Quiz from "../../components/Quiz";
import Category from "../../components/Categories/Category";

const Hello = ({ el, setInvertBackground }) => {
  let [show, setShow] = useState(true);
  let [result_title, setResultTitle] = useState("");
  let [backButton, setBackButton] = useState(false);

  const backButtonFunction = () => {
    setShow(true);
    setBackButton(false);
  };

  return (
    <>
      <Title
        backButton={backButton}
        result_title={result_title}
        backButtonFunction={backButtonFunction}
      />
      {show ? (
        <div>
          <Tip txt="Wybrana kategoria: " theme={el.title} />
          <Category icon={el.icon} title={el.title} cat={"cat"} />
          <div className="buttonWrapper">
            <button
              className={`startButton__${el.title} start_quiz`}
              onClick={() => {
                setShow(false);
              }}
            >
              <p>Rozpocznij</p>
              <div className={`startButton__${el.title}--image`}></div>
            </button>
          </div>
        </div>
      ) : (
        <Quiz
          category={el.title}
          icon={el.icon}
          title={el.title}
          cat={"cat"}
          setResultTitle={setResultTitle}
          setBackButton={setBackButton}
          setInvertBackground={setInvertBackground}
        />
      )}
    </>
  );
};
export default Hello;
