import React, { useState } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

const DragAndDropQuestionPlace = ({
  question,
  category,
  counter,
  setCounter,
  correctCounter,
  setCorrectCounter,
  setResult,
}) => {
  const [actualAnswers, updateActualAnswers] = useState(question.answers);
  const [choice, setChoice] = useState([]);
  let [disabled, setDisabled] = useState(false);

  let [isGood, setIsGood] = useState(false);
  let [isBad, setIsBad] = useState(false);

  function handleOnDragEnd(result) {
    if (!result.destination) return;
    if (result.destination.droppableId === "answer") {
      const items = actualAnswers.slice();

      const newChoice = choice.slice();
      newChoice.push(items[result.source.index]);
      setChoice(newChoice);
      delete items[result.source.index];
      updateActualAnswers(items);
      check(actualAnswers[result.source.index]);
    }
  }

  const check = (answer) => {
    if (answer === question.correct) {
      setIsGood(true);
      setDisabled(true);
      setCorrectCounter(correctCounter + 1);
      if (counter < 10) {
        setTimeout(() => {
          setCounter(counter + 1);
          setDisabled(false);
        }, 1000);
      } else {
        setTimeout(() => {
          setResult(true);
          setDisabled(false);
        }, 1000);
      }
    } else {
      setIsBad(true);
      setDisabled(true);
      if (counter < 10) {
        setTimeout(() => {
          setCounter(counter + 1);
          setDisabled(false);
        }, 1000);
      } else {
        setTimeout(() => {
          setResult(true);
          setDisabled(false);
        }, 1000);
      }
    }
  };

  return (
    <div>
      <DragDropContext onDragEnd={handleOnDragEnd}>
        <div className="QuestionDD">
          <div className="QuestionDD__content">
            <p>{question.quest[0]}</p>
          </div>
          <Droppable droppableId="answer">
            {(provided) => (
              <div
                className={`QuestionDD__DDPlace--${category}`}
                {...provided.droppableProps}
                ref={provided.innerRef}
              >
                {choice.map((el) => {
                  return (
                    <div
                      className={`QuestionDD__choice--${category} ${
                        isGood ? "good5" : ""
                      } ${isBad ? "bad5" : ""}`}
                      key={Math.random()}
                    >
                      <p>{el}</p>
                    </div>
                  );
                })}
                {choice.length === 0 ? (
                  <p className="QuestionDD__tip">PRZECIĄGNIJ I UPUŚĆ TUTAJ</p>
                ) : (
                  <div></div>
                )}
              </div>
            )}
          </Droppable>
          <div className="QuestionDD__content">
            <p>{question.quest[1]}</p>
          </div>
        </div>

        <Droppable droppableId="questions">
          {(provided) => (
            <div
              className="AnswersDD AnswersDD__2"
              {...provided.droppableProps}
              ref={provided.innerRef}
            >
              {actualAnswers.map((el, index) => {
                return (
                  <Draggable
                    isDragDisabled={disabled}
                    key={Math.random()}
                    index={index}
                    draggableId={el}
                  >
                    {(provided) => (
                      <div
                        className={`QuestionDD__choice--${category}`}
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                      >
                        <p>{el}</p>
                      </div>
                    )}
                  </Draggable>
                );
              })}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </div>
  );
};

export default DragAndDropQuestionPlace;
