import logo from "../../assets/logo.png";
import back from "../../assets/cofnij_x.svg";
import close from "../../assets/zamknij_x.svg";
import { Link } from "react-router-dom";

const Title = ({ backButton, result_title, backButtonFunction }) => {
  return (
    <div className={`main-wrapper__title ${result_title}`}>
      <div className="logo">
        <img src={logo} alt="logo" />
      </div>
      {backButton ? (
        <div
          onClick={() => {
            backButtonFunction();
          }}
        >
          <img className="navButton navButtonBack" src={back} alt="back" />
        </div>
      ) : (
        <div></div>
      )}
      <div>
        <Link to="/">
          <img
            className={`navButton navButtonClose ${
              result_title === undefined || result_title === ""
                ? ""
                : "navButtonCloseResult"
            }`}
            src={close}
            alt="close"
          />
        </Link>
      </div>
    </div>
  );
};

export default Title;
