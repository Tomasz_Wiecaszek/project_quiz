const Category = ({ icon, title, cat, theme, anotherCat }) => {
  return (
    <div
      className={`category__${theme} ${cat} ${anotherCat}`}
      key={Math.random()}
    >
      <div className={`category__container `}>
        <img src={icon} className="category__img" alt="Image_of_Category" />
        <hr />
        {title}
      </div>
    </div>
  );
};

export default Category;
