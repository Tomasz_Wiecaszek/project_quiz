import { myCatList } from "../../elements/index";
import { Link } from "react-router-dom";

import Category from "./Category";

const Categories = ({ title, noWrap, anotherCat }) => {
  return (
    <div className={`main-wrapper__categories ${noWrap}`}>
      {myCatList.map((el) => {
        return (
          <Link to={el.link} key={Math.random()}>
            {title !== el.title ? (
              <Category
                icon={el.icon}
                title={el.title}
                key={el.icon}
                theme={title}
                anotherCat={anotherCat}
              />
            ) : (
              <div></div>
            )}
          </Link>
        );
      })}
    </div>
  );
};

export default Categories;
