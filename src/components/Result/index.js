import ResultCircle from "../ResultCircle";
import Category from "../Categories/Category";
import Tip from "../Tip";
import Categories from "../Categories";

const Result = ({
  theme,
  result,
  icon,
  title,
  resetQuiz,
  setResultTitle,
  setBackButton,
  setInvertBackground,
}) => {
  setInvertBackground(false);
  setResultTitle("result_title");
  setBackButton(false);
  return (
    <div className="result-wrapper">
      <div className="result-wrapper__result">
        <Category icon={icon} title={title} cat={"cat"} />
        <div className="result-wrapper__yourResult">
          <Tip txt="TWÓJ WYNIK " theme={theme} endResultTip="endResultTip" />
          <ResultCircle
            theme={theme}
            result={result}
            endResultCircle="endResultCircle"
          />
        </div>

        <button
          className={`startButton__${theme} start_quiz`}
          onClick={() => {
            resetQuiz();
          }}
        >
          <p>POWTÓRZ QUIZ</p>
          <div className={`startButton__${theme}--image returnQuiz`}></div>
        </button>
      </div>
      <div className="result-wrapper__category">
        <p>WYBIERZ INNĄ KATEGORIĘ:</p>
        <Categories title={theme} anotherCat="anotherCat" noWrap="noWrap" />
      </div>
    </div>
  );
};

export default Result;
